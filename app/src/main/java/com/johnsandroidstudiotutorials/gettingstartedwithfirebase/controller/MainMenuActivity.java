package com.johnsandroidstudiotutorials.gettingstartedwithfirebase.controller;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.google.firebase.messaging.FirebaseMessaging;
import com.johnsandroidstudiotutorials.gettingstartedwithfirebase.R;


public class MainMenuActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView button_start,button_login;
    private Context context;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_menu_layout);


        context = this;
        button_start = (TextView) findViewById(R.id.button_start);
        button_start.setOnClickListener(this);
        button_login = (TextView) findViewById(R.id.button_login);
        button_login.setOnClickListener(this);


    }



    @Override
    public void onClick(View view) {
        Intent intent = null;
        switch (view.getId()) {
            //Handle Clicks
            case R.id.button_start:
                intent = new Intent(context, EnterAnonymousQuizActivity.class);
                break;

            case R.id.button_login:
                intent = new Intent(context, LoginActivity.class);
                break;

            default:
                break;
        }
        if(intent != null) {
            startActivity(intent);
        }
    }



}
