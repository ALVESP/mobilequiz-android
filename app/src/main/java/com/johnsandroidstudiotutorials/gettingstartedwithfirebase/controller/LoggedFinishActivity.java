package com.johnsandroidstudiotutorials.gettingstartedwithfirebase.controller;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.johnsandroidstudiotutorials.gettingstartedwithfirebase.R;

/**
 * Created by Pedro on 29/06/2017.
 */

public class LoggedFinishActivity extends AppCompatActivity implements View.OnClickListener {
    private Button button_finish2;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.finish_layout);

        context = this;
        button_finish2= (Button) findViewById(R.id.button_finish);
        button_finish2.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent intent = null;
        switch (view.getId()) {
            //Handle Clicks
            case R.id.button_finish:
                intent = new Intent(context, EndLoggedQuizActivity.class);
                break;

            default:
                break;
        }
        if(intent != null) {
            startActivity(intent);
        }
    }
}

