package com.johnsandroidstudiotutorials.gettingstartedwithfirebase.controller;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.johnsandroidstudiotutorials.gettingstartedwithfirebase.R;

/**
 * Created by Pedro on 29/06/2017.
 */

public class EndLoggedQuizActivity extends AppCompatActivity{
    Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.end_layout);

        handler.postDelayed(new Runnable(){
            @Override
            public void run(){
                Intent i = new Intent(EndLoggedQuizActivity.this, MainMenuActivity.class);
                startActivity(i);
            }
        }, 3000);





    }


    }


