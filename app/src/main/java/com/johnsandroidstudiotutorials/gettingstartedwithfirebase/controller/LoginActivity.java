package com.johnsandroidstudiotutorials.gettingstartedwithfirebase.controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.messaging.FirebaseMessaging;
import com.johnsandroidstudiotutorials.gettingstartedwithfirebase.R;

import static com.johnsandroidstudiotutorials.gettingstartedwithfirebase.R.id.button_login2;

/**
 * Created by Pedro on 01/06/2017.
 */

public class LoginActivity extends AppCompatActivity  {

    EditText txtutilizador, txtpalavrapasse;
    FirebaseAuth  firebaseAuth;

    Button button_login2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_layout);

        txtutilizador = (EditText) findViewById(R.id.txtutilizador);
        txtpalavrapasse = (EditText) findViewById(R.id.txtpalavrapasse);
        firebaseAuth = FirebaseAuth.getInstance();
        button_login2 = (Button) findViewById(R.id.button_login2);
    }


    public void button_login2_Click(View view) {

        (firebaseAuth.signInWithEmailAndPassword(txtutilizador.getText().toString(), txtpalavrapasse.getText().toString()))
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            Toast.makeText(LoginActivity.this, "Login Successful", Toast.LENGTH_LONG).show();
                            Intent i = new Intent(LoginActivity.this, LoggedMainMenuActivity.class);
                            i.putExtra("Email", firebaseAuth.getCurrentUser().getEmail());
                            startActivity(i);

                        } else {
                            Log.e("ERROR", task.getException().toString());
                            Toast.makeText(LoginActivity.this, task.getException().getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }
}
