package com.johnsandroidstudiotutorials.gettingstartedwithfirebase.controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.firebase.auth.FirebaseAuth;
import com.johnsandroidstudiotutorials.gettingstartedwithfirebase.R;

public class LoggedQuizActivity extends AppCompatActivity {

    TextView Enunciado, Numero_pergunta, user_id;
    Button button_A, button_B, button_C, button_D, button_next;
    Firebase enunciadoReference, respostaReference, AReference, BReference, CReference, DReference,EndReference;
    FirebaseAuth firebaseAuth;
    int a;
    int i;
    int perguntas[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.logged_quiz_layout);
    }

    @Override
    protected void onStart() {
        super.onStart();

        user_id = (TextView) findViewById(R.id.user_id);
        user_id.setText(getIntent().getExtras().getString("Email").replaceAll("@hotmail.com","").replaceAll("@gmail.com","").replaceAll("@alunos.ulht.pt","").replaceAll("@alunos.ulusofona.pt","").replaceAll("@admin.pt",""));
        Numero_pergunta = (TextView) findViewById(R.id.Numero_pergunta);
        Enunciado = (TextView) findViewById(R.id.Enunciado);
        button_A = (Button) findViewById(R.id.button_A);
        button_B = (Button) findViewById(R.id.button_B);
        button_C = (Button) findViewById(R.id.button_C);
        button_D = (Button) findViewById(R.id.button_D);
        button_next = (Button) findViewById(R.id.button_next);

        enunciadoReference = new Firebase("https://fir-mobilequizz.firebaseio.com/perguntas/"+perguntas[i]+"/Enunciado");

        respostaReference = new Firebase("https://fir-mobilequizz.firebaseio.com/perguntas/"+perguntas[i]+"/RespostaAutenticada/");
        AReference = new Firebase("https://fir-mobilequizz.firebaseio.com/perguntas/"+perguntas[i]+"/A");
        BReference = new Firebase("https://fir-mobilequizz.firebaseio.com/perguntas/"+perguntas[i]+"/B");
        CReference = new Firebase("https://fir-mobilequizz.firebaseio.com/perguntas/"+perguntas[i]+"/C");
        DReference = new Firebase("https://fir-mobilequizz.firebaseio.com/perguntas/"+perguntas[i]+"/D");
        Numero_pergunta.setText(""+perguntas[i]);
        EndReference = new Firebase("https://fir-mobilequizz.firebaseio.com/perguntas/"+perguntas[i]+"/Enunciado");



        EndReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String text = dataSnapshot.getValue(String.class);
                if(text==null){
                    Intent i = new Intent(LoggedQuizActivity.this, LoggedFinishActivity.class);
                    startActivity(i);
                }

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });




        enunciadoReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String text = dataSnapshot.getValue(String.class);
                Enunciado.setText(text);

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

        AReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String text = dataSnapshot.getValue(String.class);
                button_A.setText(text);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

        BReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String text = dataSnapshot.getValue(String.class);
                button_B.setText(text);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

        CReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String text = dataSnapshot.getValue(String.class);
                button_C.setText(text);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
        DReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String text = dataSnapshot.getValue(String.class);
                button_D.setText(text);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

        button_A.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                button_next.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        respostaReference.child(getIntent().getExtras().getString("Email").replaceAll("@hotmail.com","").replaceAll("@gmail.com","").replaceAll("@alunos.ulht.pt","").replaceAll("@alunos.ulusofona.pt","").replaceAll("@admin.pt","")).setValue("A");
                        if(i+1==perguntas.length){
                            Intent i = new Intent(LoggedQuizActivity.this, LoggedFinishActivity.class);
                            startActivity(i);
                        } else{
                            i++;
                            onStart();
                        }

                    }
                });
            }
        });
        button_B.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                button_next.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        respostaReference.child(getIntent().getExtras().getString("Email").replaceAll("@hotmail.com","").replaceAll("@gmail.com","").replaceAll("@alunos.ulht.pt","").replaceAll("@alunos.ulusofona.pt","").replaceAll("@admin.pt","")).setValue("B");
                        if(i+1==perguntas.length){
                            Intent i = new Intent(LoggedQuizActivity.this, LoggedFinishActivity.class);
                            startActivity(i);
                        } else{
                            i++;
                            onStart();
                        }

                    }
                });

            }
        });
        button_C.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                button_next.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        respostaReference.child(getIntent().getExtras().getString("Email").replaceAll("@hotmail.com","").replaceAll("@gmail.com","").replaceAll("@alunos.ulht.pt","").replaceAll("@alunos.ulusofona.pt","").replaceAll("@admin.pt","")).setValue("C");
                        if(i+1==perguntas.length){
                            Intent i = new Intent(LoggedQuizActivity.this, LoggedFinishActivity.class);
                            startActivity(i);
                        } else{
                            i++;
                            onStart();
                        }

                    }
                });
            }
        });
        button_D.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                button_next.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                       respostaReference.child(getIntent().getExtras().getString("Email").replaceAll("@hotmail.com","").replaceAll("@gmail.com","").replaceAll("@alunos.ulht.pt","").replaceAll("@alunos.ulusofona.pt","").replaceAll("@admin.pt","")).setValue("D");
                            if(i+1==perguntas.length){
                                Intent i = new Intent(LoggedQuizActivity.this, LoggedFinishActivity.class);
                                startActivity(i);
                            } else{
                                i++;
                                onStart();
                            }

                        }
                    });

            }
        });
    }
}