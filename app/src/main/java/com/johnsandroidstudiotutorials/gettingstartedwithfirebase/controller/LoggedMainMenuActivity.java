package com.johnsandroidstudiotutorials.gettingstartedwithfirebase.controller;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import com.google.firebase.messaging.FirebaseMessaging;
import com.johnsandroidstudiotutorials.gettingstartedwithfirebase.R;

/**
 * Created by Pedro on 10/06/2017.
 */

public class LoggedMainMenuActivity extends AppCompatActivity implements View.OnClickListener {

    private Button button_start2;
    private Context context;
    private FirebaseAuth firebaseAuth;
    Firebase disciplinaReference1,disciplinaReference2,disciplinaReference3,disciplinaReference4,disciplinaReference5,disciplinaReference6,disciplinaReference7;
    String TAG;
    int o;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.logged_main_menu_layout);

        context = this;
        button_start2 = (Button) findViewById(R.id.button_start2);
        button_start2.setOnClickListener(this);

    }

    @Override
    protected void onStart() {
        super.onStart();

        disciplinaReference1 = new Firebase("https://fir-mobilequizz.firebaseio.com/alunos/" + getIntent().getExtras().getString("Email").replaceAll("@hotmail.com", "").replaceAll("@gmail.com", "").replaceAll("@alunos.ulht.pt", "").replaceAll("@alunos.ulusofona.pt", "").replaceAll("@admin.pt", "") + "/Unidade1");
        disciplinaReference2 = new Firebase("https://fir-mobilequizz.firebaseio.com/alunos/" + getIntent().getExtras().getString("Email").replaceAll("@hotmail.com", "").replaceAll("@gmail.com", "").replaceAll("@alunos.ulht.pt", "").replaceAll("@alunos.ulusofona.pt", "").replaceAll("@admin.pt", "") + "/Unidade2");
        disciplinaReference3 = new Firebase("https://fir-mobilequizz.firebaseio.com/alunos/" + getIntent().getExtras().getString("Email").replaceAll("@hotmail.com", "").replaceAll("@gmail.com", "").replaceAll("@alunos.ulht.pt", "").replaceAll("@alunos.ulusofona.pt", "").replaceAll("@admin.pt", "") + "/Unidade3");
        disciplinaReference4 = new Firebase("https://fir-mobilequizz.firebaseio.com/alunos/" + getIntent().getExtras().getString("Email").replaceAll("@hotmail.com", "").replaceAll("@gmail.com", "").replaceAll("@alunos.ulht.pt", "").replaceAll("@alunos.ulusofona.pt", "").replaceAll("@admin.pt", "") + "/Unidade4");
        disciplinaReference5 = new Firebase("https://fir-mobilequizz.firebaseio.com/alunos/" + getIntent().getExtras().getString("Email").replaceAll("@hotmail.com", "").replaceAll("@gmail.com", "").replaceAll("@alunos.ulht.pt", "").replaceAll("@alunos.ulusofona.pt", "").replaceAll("@admin.pt", "") + "/Unidade5");
        disciplinaReference6 = new Firebase("https://fir-mobilequizz.firebaseio.com/alunos/" + getIntent().getExtras().getString("Email").replaceAll("@hotmail.com", "").replaceAll("@gmail.com", "").replaceAll("@alunos.ulht.pt", "").replaceAll("@alunos.ulusofona.pt", "").replaceAll("@admin.pt", "") + "/Unidade6");
        disciplinaReference7 = new Firebase("https://fir-mobilequizz.firebaseio.com/alunos/" + getIntent().getExtras().getString("Email").replaceAll("@hotmail.com", "").replaceAll("@gmail.com", "").replaceAll("@alunos.ulht.pt", "").replaceAll("@alunos.ulusofona.pt", "").replaceAll("@admin.pt", "") + "/Unidade7");


        disciplinaReference1.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String disciplina1 = dataSnapshot.getValue(String.class);
                FirebaseMessaging.getInstance().subscribeToTopic(disciplina1);
                Log.d(TAG, "Subscrivi-me a =" + disciplina1);

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

        disciplinaReference2.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String disciplina2 = dataSnapshot.getValue(String.class);
                FirebaseMessaging.getInstance().subscribeToTopic(disciplina2);
                Log.v(TAG, "Subscrivi-me a =" + disciplina2);

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

        disciplinaReference3.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String disciplina3 = dataSnapshot.getValue(String.class);
                FirebaseMessaging.getInstance().subscribeToTopic(disciplina3);
                Log.v(TAG, "Subscrivi-me a =" + disciplina3);

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

            disciplinaReference4.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    String disciplina4 = dataSnapshot.getValue(String.class);
                    FirebaseMessaging.getInstance().subscribeToTopic(disciplina4);
                    Log.v(TAG, "Subscrivi-me a =" + disciplina4);

                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {

                }
            });

            disciplinaReference5.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    String disciplina5 = dataSnapshot.getValue(String.class);
                    FirebaseMessaging.getInstance().subscribeToTopic(disciplina5);
                    Log.v(TAG, "Subscrivi-me a =" + disciplina5);

                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {

                }
            });

            disciplinaReference6.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    String disciplina6 = dataSnapshot.getValue(String.class);
                    FirebaseMessaging.getInstance().subscribeToTopic(disciplina6);
                    Log.v(TAG, "Subscrivi-me a =" + disciplina6);

                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {

                }
            });

            disciplinaReference7.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    String disciplina7 = dataSnapshot.getValue(String.class);
                    FirebaseMessaging.getInstance().subscribeToTopic(disciplina7);
                    Log.v(TAG, "Subscrivi-me a =" + disciplina7);

                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {

                }
            });





    }

        @Override
        public void onClick (View view){
            Intent intent = null;
            switch (view.getId()) {
                //Handle Clicks
                case R.id.button_start2:
                    Intent i = new Intent(LoggedMainMenuActivity.this, EnterLoggedQuizActivity.class);
                    i.putExtra("Email", getIntent().getExtras().getString("Email"));
                    startActivity(i);
                    break;

                default:
                    break;
            }
            if (intent != null) {
                startActivity(intent);
            }
        }
    }
