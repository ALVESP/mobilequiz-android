package com.johnsandroidstudiotutorials.gettingstartedwithfirebase.controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.johnsandroidstudiotutorials.gettingstartedwithfirebase.R;

/**
 * Created by Pedro on 05/07/2017.
 */

public class EnterLoggedQuizActivity extends AppCompatActivity {

    EditText quizpassword;
    Button button_quiz_pass;
    Firebase passwordReference;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.enter_password_layout);


    }

    @Override
    protected void onStart() {

        super.onStart();
        quizpassword = (EditText) findViewById(R.id.quizpassword);
        button_quiz_pass = (Button) findViewById(R.id.button_quiz_pass);
        passwordReference = new Firebase("https://fir-mobilequizz.firebaseio.com/Password");
        button_quiz_pass.setOnClickListener(new View.OnClickListener(){


            @Override
            public void onClick(View view) {

                passwordReference.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String text = dataSnapshot.getValue(String.class).toString();
                        String passInserida = quizpassword.getText().toString();
                        Log.d("TAG", "passInserida---"+passInserida);
                        Log.d("TAG", "text---"+text);
                        if(passInserida.equals(text)){
                            Toast.makeText(EnterLoggedQuizActivity.this, "Password Correcta", Toast.LENGTH_LONG).show();
                            Intent i = new Intent(EnterLoggedQuizActivity.this, LoggedQuizActivity.class);
                            i.putExtra("Email", getIntent().getExtras().getString("Email"));
                            startActivity(i);
                        }
                        else{
                            Toast.makeText(EnterLoggedQuizActivity.this, "Password Incorreta", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onCancelled(FirebaseError firebaseError) {

                    }
                });

            }

        });

    }
}
